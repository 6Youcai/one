#include <iostream>
#include <fstream>
#include <zlib.h>
#include "kseq.h"
#include "cmdline.h"
// g++ -std=c++11 my.cpp -lz -o filter

using namespace std;
const int Phred = 33;

struct ReadInfo // single end
{
  int base;
  int qual;
  int q20;
  int q30;
  int gc_num;
  int gc_con;
};

struct PairInfo // pair end
{
  int base;
  int qual[2];
  int q20;
  int q30;
  int gc_num;
  int gc_con[2];
};

struct Statistics // raw and clean
{
  long reads[2];
  long base[2];
  long q20[2];
  long q30[2];
  long gc_num[2];
  long gc_con[101][2];
};

KSEQ_INIT(gzFile, gzread);
cmdline::parser parameter(int argc, char *argv[]);
ReadInfo read_check(const kseq_t *seq);
PairInfo *pair_check(const kseq_t *seq1, const kseq_t *seq2);
void write_seq(const kseq_t *seq, gzFile file);
void update_stat(Statistics *stat, const PairInfo *pair, int i);
void gc_distribution(const Statistics *stat, string fname);
void out_stat(const Statistics *stat, string fname);

int main(int argc, char *argv[])
{
  // parameter
  cmdline::parser opt = parameter(argc, argv);
  gzFile fp1, fp2, out1, out2;
  fp1 = gzopen(opt.get<string>("read1").c_str(), "r");
  fp2 = gzopen(opt.get<string>("read2").c_str(), "r");
  out1 = gzopen(opt.get<string>("out1").c_str(), "wb");
  out2 = gzopen(opt.get<string>("out2").c_str(), "wb");
  int cutoff = opt.get<int>("cutoff");
  string fname = opt.get<string>("gc");
  string fname2 = opt.get<string>("stat");

  // name.s, comment.s, seq.s, qual.s, seq.l
  kseq_t *seq1 = kseq_init(fp1);
  kseq_t *seq2 = kseq_init(fp2);

  Statistics info = {{0}, {0}, {0}, {0}, {0}, {{0}, {0}}};
  Statistics *stat = &info;
  PairInfo *pair;

  int l;
  while ((l = kseq_read(seq1)) >= 0)
  {
    kseq_read(seq2);

    pair = pair_check(seq1, seq2);
    update_stat(stat, pair,  0); // raw

    if (pair->qual[0] >= cutoff and pair->qual[1] >= cutoff)
    {
      update_stat(stat, pair,  1); // clean
      write_seq(seq1, out1);
      write_seq(seq2, out2);
    }
  }

  kseq_destroy(seq1); kseq_destroy(seq2);
  gzclose(fp1); gzclose(fp2);
  gzclose(out1); gzclose(out2);

  out_stat(stat, fname2);
  gc_distribution(stat, fname);

  return 0;
}

// function
cmdline::parser parameter(int argc, char *argv[])
{
  // long name, short name, description, is mandatory, default value
  cmdline::parser opt;
  opt.add<string>("read1", '1', "input read1", true, "");
  opt.add<string>("read2", '2', "input read2", true, "");
  opt.add<string>("out1", '3', "out read1", true, "");
  opt.add<string>("out2", '4', "out read2", true, "");
  opt.add<int>("cutoff", 'c', "mean reads quality cut off, default is 30", false, 30);
  opt.add<string>("gc", '\0', "gc content file name", false, "gc.distribution.txt");
  opt.add<string>("stat", '\0', "read statistics file name", false, "stat.txt");
  opt.parse_check(argc, argv);
  return opt;
}

ReadInfo read_check(const kseq_t *seq) // single end
{
  char *sequence = seq->seq.s;
  char *quality = seq->qual.s;
  int Q, sumQ = 0;

  ReadInfo info = {0, 0, 0, 0, 0, 0};
  info.base = seq->seq.l; // base number

  for (; *sequence or *quality; sequence++, quality++)
  {
    if (*sequence == 'G' or *sequence == 'C')
      info.gc_num += 1; // gc base number
    Q = *quality - Phred;
    sumQ += Q; //
    if (Q >= 20)
    {
      info.q20 += 1; // q20 base number
      if (Q >= 30)
        info.q30 += 1; // q30 base number
    }
  }
  info.qual = sumQ / seq->seq.l; // read average quality
  info.gc_con = 100 * info.gc_num / seq->seq.l; // read gc content

  return info;
}

PairInfo *pair_check(const kseq_t *seq1, const kseq_t *seq2)
{
  ReadInfo read1 = read_check(seq1);
  ReadInfo read2 = read_check(seq2);

  PairInfo info;
  info.base = read1.base + read2.base;
  info.qual[0] = read1.qual;
  info.qual[1] = read2.qual;
  info.q20 = read1.q20 + read2.q20;
  info.q30 = read1.q30 + read2.q30;
  info.gc_num = read1.gc_num + read2.gc_num;
  info.gc_con[0] = read1.gc_con;
  info.gc_con[1] = read2.gc_con;

  PairInfo *p = &info;
  return p;
}

void update_stat(Statistics *stat, const PairInfo *pair, int i)
{
  stat->reads[i] += 1;
  stat->base[i] += pair->base;
  stat->q20[i] += pair->q20;
  stat->q30[i] += pair->q30;
  stat->gc_num[i] += pair->gc_num;
  stat->gc_con[pair->gc_con[0]][i] += 1;
  stat->gc_con[pair->gc_con[1]][i] += 1;
}

void write_seq(const kseq_t *seq, gzFile file)
{
  gzputs(file, seq->name.s); // read name
  gzputc(file, ' ');
  gzputs(file, seq->comment.s); // index
  gzputc(file, '\n');
  gzputs(file, seq->seq.s); // seq
  gzputc(file, '\n');
  gzputs(file, "+\n"); // +
  gzputs(file, seq->qual.s); // qual
  gzputc(file, '\n');
}

void out_stat(const Statistics *stat, string fname)
{
  ofstream f;
  f.open(fname);
  f << "raw_reads: " << stat->reads[0] << endl
       << "clean_reads: " << stat->reads[1] << endl;
  f << "raw_base : " << stat->base[0] << endl
       << "clean_base: " << stat->base[1] << endl;
  f << "raw_gc: " << 100 * stat->gc_num[0] /  stat->base[0] << endl
       << "clean_gc: " << 100 * stat->gc_num[1] /  stat->base[1] << endl;
  f << "raw_q20: " << 100 * stat->q20[0] / stat->base[0] << endl
       << "clean_q20: " << 100 * stat->q20[1] / stat->base[1] << endl;
  f << "raw_q30: " << 100 * stat->q30[0] / stat->base[0] << endl
       << "clean_q30: " << 100 * stat->q30[1] / stat->base[1] << endl;
  f.close();
}

void gc_distribution(const Statistics *stat, string fname)
{
  ofstream f;
  f.open(fname);
  f << "GC\tRaw\tClean" << endl;
  for (int i = 0; i <= 100; i++)
    f << i << "\t" << stat->gc_con[i][0] << "\t" << stat->gc_con[i][1] << endl;
  f.close();
}
