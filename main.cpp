#include <iostream>
#include <fstream>
#include <zlib.h>
#include "kseq.h"
#include "cmdline.h"
// g++ -std=c++11 main.cpp -lz -o filter

using namespace std;
const int Phred = 33;
KSEQ_INIT(gzFile, gzread);

struct nw {
  // alignment result
  int score;
  int step;
  int start_x;
  int start_y;
};

char complement(const char base);
string reverse_complement(const char *read2);
void write_seq(const kseq_t *seq, gzFile file);
nw alignment(const kseq_t *seq1, const kseq_t *seq2, const char *read3);
void correction(kseq_t *seq1, kseq_t *seq2, const char *read3, const nw res);
void overlap_correction(kseq_t *seq1, kseq_t *seq2);
cmdline::parser parameter(int argc, char *argv[]);
int read_check(const kseq_t *seq);
bool pair_check(const kseq_t *seq1, const kseq_t *seq2, int Q);

int main(int argc, char *argv[]) {
  cmdline::parser opt = parameter(argc, argv);
  gzFile fp1, fp2, out1, out2;
  fp1 = gzopen(opt.get<string>("read1").c_str(), "r");
  fp2 = gzopen(opt.get<string>("read2").c_str(), "r");
  out1 = gzopen(opt.get<string>("out1").c_str(), "wb");
  out2 = gzopen(opt.get<string>("out2").c_str(), "wb");
  int qual = opt.get<int>("qual");

  kseq_t *seq1 = kseq_init(fp1);
  kseq_t *seq2 = kseq_init(fp2);
  bool correction = opt.exist("correct");

  while (kseq_read(seq1) >= 0) {
      kseq_read(seq2);
      bool qc = pair_check(seq1, seq2, qual);
      if (not qc)
        continue;
      if (correction)
        overlap_correction(seq1, seq2);
      write_seq(seq1, out1);
      write_seq(seq2, out2);
  }

  kseq_destroy(seq1); gzclose(fp1);
  kseq_destroy(seq2); gzclose(fp2);
  gzclose(out1); gzclose(out2);

  return 0;
}

// function

char complement(const char base) {
  char base2;
  switch (base) {
    case 'A': base2 = 'T'; break;
    case 'T': base2 = 'A'; break;
    case 'C': base2 = 'G'; break;
    case 'G': base2 = 'C'; break;
    case 'N': base2 = 'N'; break;
  }
  return base2;
}

string reverse_complement(const char *read2) {
  int i = strlen(read2);
  string sequence(i, 0);
  for (; *read2; read2++, i--) {
    char base = *read2;
    sequence[i-1] = complement(base);
  }
  return sequence;
}

void write_seq(const kseq_t *seq, gzFile file) {
  // out put reads
  gzputc(file, '@');
  gzputs(file, seq->name.s);
  gzputc(file, ' ');
  gzputs(file, seq->comment.s);
  gzputc(file, '\n');
  gzputs(file, seq->seq.s);
  gzputc(file, '\n');
  gzputs(file, "+\n");
  gzputs(file, seq->qual.s);
  gzputc(file, '\n');
}

nw alignment(const kseq_t *seq1, const kseq_t *seq2, const char *read3) {
  // Needleman-Wunsch
  int penalty = 0, match = 1, mismatch = -1;
  int L1 = seq1->seq.l, L2 = seq2->seq.l;
  int Mat[L1+1][L2+1], i, j;
  char *read1 = seq1->seq.s;
  for (i = 0; i <= L1; i++)
    Mat[i][0] = penalty;
  for (j = 0; j <= L2; j++)
    Mat[0][j] = penalty;
  int score = 0, delt, x = 0, y = 0;
  for (i = 1; i <= L1; i++) {
    for (j = 1; j <= L2; j ++) {
      delt = read1[i-1] == read3[j-1]? match: mismatch;
      Mat[i][j] = Mat[i-1][j-1] + delt;
      if (Mat[i][j] > score) {
        score = Mat[i][j];
        x = i; y = j;
      }
    }
  }
  int start_x, start_y, step;
  if (x >= y) {
    start_x = x - y; start_y = 0;
    step = L1 - start_x;
  } else {
    start_x = 0; start_y = y - x;
    step = L2 - start_y;
  }
  nw res = {score, step, start_x, start_y};
  return res;
}

void correction(kseq_t *seq1, kseq_t *seq2, const char *read3, const nw res) {
  char *read1 = seq1->seq.s;
  char *read2 = seq2->seq.s;
  char *qual1 = seq1->qual.s;
  char *qual2 = seq2->qual.s;
  int L1 = seq1->seq.l;
  int L2 = seq2->seq.l;
  int start_x = res.start_x, start_y = res.start_y;
  for (int s = 0; s < res.step; s++) {
    char b1 = read1[start_x + s];
    char b3 = read3[start_y + s];
    if (b1 != b3) {
      int q1 = qual1[start_x + s] - Phred;
      int q2 = qual2[L2 - 1 - start_y - s] - Phred;
      // cut off, hard code
      if (q1 >= 30 and q2 <= 20) {
        // change read2
        qual2[L2-1 - start_y-s] = q1 + Phred;
        read2[L2-1 - start_y-s] = complement(b1);
      } else if (q1 <= 20 and q2 >= 30) {
        // change read1
        qual1[start_x + s] = q2 + Phred;
        read1[start_x + s] = b3;
      }
    }
  }
}

void overlap_correction(kseq_t *seq1, kseq_t *seq2) {
  char *read2 = seq2->seq.s;
  string sequence2 = reverse_complement(read2);
  const char *read3 = sequence2.c_str();
  nw res = alignment(seq1, seq2, read3);
  // cut off, hard code
  if (res.score != res.step and res.score >= 10) {
    correction(seq1, seq2, read3, res);
  }
}

cmdline::parser parameter(int argc, char *argv[])
{
  // long name, short name, description, # is mandatory, default value
  cmdline::parser opt;
  opt.add<string>("read1", '1', "input read1", true, "");
  opt.add<string>("read2", '2', "input read2", true, "");
  opt.add<string>("out1", '3', "out read1", true, "");
  opt.add<string>("out2", '4', "out read2", true, "");
  opt.add<int>("qual", 'q', "mean reads quality cut off, default is 30", false, 30);
  opt.add("correct", 'c', "if specified, correct bases in overlapped regions, default no");
  opt.parse_check(argc, argv);
  return opt;
}

int read_check(const kseq_t *seq) {
  // single end
  char *quality = seq->qual.s;
  int Q, sumQ = 0;
  for (; *quality; quality++)
    sumQ += *quality - Phred;
  Q = sumQ / seq->seq.l;
  return Q;
}

bool pair_check(const kseq_t *seq1, const kseq_t *seq2, int Q) {
  // pair end
  int q1 = read_check(seq1);
  int q2 = read_check(seq2);
  if (q1 >= Q and q2 >= Q)
    return true;
  else
    return false;
}
